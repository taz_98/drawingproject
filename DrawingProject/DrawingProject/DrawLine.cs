﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProject
{

    //DrawLine Class created to make the compiling of code easier when carrying out basic tasks
    //inherits the commandline class to use the drawgraphic method which will put the item to paper
    class DrawLine : CommandLine
    {
        Image g;
        int width, height;

        //inherits the base Commandline method to set the starting point for the program
        public DrawLine() : base()
        {
            width = 100;
            height = 100;
        }

        //retreives the x and y values and sets them to 'this' x and y variable
        public DrawLine(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        //Then proceeds to use the variables to produce the graphic
        public override void set(Image Finalgraphic, params int[] line)
        {
            this.g = Finalgraphic;
            base.set(Finalgraphic, line[0], line[1]);
            this.x = line[2];
            this.y = line[3];
        }

        public override void drawGraphics(Image Finalgraphic)
        {
            Finalgraphic.DrawLine(this.x, this.y);
        }
    }
}
