﻿using System.Windows.Forms;
namespace DrawingProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommandInput = new System.Windows.Forms.TextBox();
            this.ProgrammingLine = new System.Windows.Forms.RichTextBox();
            this.Canvass = new System.Windows.Forms.PictureBox();
            this.Load = new System.Windows.Forms.OpenFileDialog();
            this.Save = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Canvass)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandInput
            // 
            this.CommandInput.Location = new System.Drawing.Point(13, 597);
            this.CommandInput.Name = "CommandInput";
            this.CommandInput.Size = new System.Drawing.Size(1295, 20);
            this.CommandInput.TabIndex = 0;
            this.CommandInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandInput_KeyDown);
            // 
            // ProgrammingLine
            // 
            this.ProgrammingLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProgrammingLine.Location = new System.Drawing.Point(12, 21);
            this.ProgrammingLine.Name = "ProgrammingLine";
            this.ProgrammingLine.Size = new System.Drawing.Size(597, 570);
            this.ProgrammingLine.TabIndex = 1;
            this.ProgrammingLine.Text = "";
            this.ProgrammingLine.TextChanged += new System.EventHandler(this.ProgrammingLine_TextChanged);
            // 
            // Canvass
            // 
            this.Canvass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Canvass.Location = new System.Drawing.Point(648, 21);
            this.Canvass.Name = "Canvass";
            this.Canvass.Size = new System.Drawing.Size(660, 570);
            this.Canvass.TabIndex = 2;
            this.Canvass.TabStop = false;
            this.Canvass.Paint += new System.Windows.Forms.PaintEventHandler(this.Canvass_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1320, 629);
            this.Controls.Add(this.Canvass);
            this.Controls.Add(this.ProgrammingLine);
            this.Controls.Add(this.CommandInput);
            this.Name = "Form1";
            this.Text = "Drawing Program";
            ((System.ComponentModel.ISupportInitialize)(this.Canvass)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Canvass_Click(object sender, System.EventArgs e)
        {

        }

        #endregion

        private System.Windows.Forms.TextBox CommandInput;
        private System.Windows.Forms.RichTextBox ProgrammingLine;
        private System.Windows.Forms.PictureBox Canvass;
        private OpenFileDialog Load;
        private SaveFileDialog Save;
    }
}

