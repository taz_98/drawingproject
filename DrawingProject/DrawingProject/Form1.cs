﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProject
{
    public partial class Form1 : Form
    {
        //Bitmap PictureBitmap = new Bitmap(660, 570);
        MemoryStream stream = new MemoryStream();

        Authenticator authenticate;
        public int LineNumber;
        Parser parse = new Parser();
        Image Finalgraphics;

        public Form1()
        {
            InitializeComponent(); //Creates and initialises the components of the Windows form application
            authenticate = new Authenticator();
            Finalgraphics = new Image(); // creates an image using the bitmap initialised 'PictureBitmap'
        }

        //
        //Method to Detect key press in CommandInput textbox
        //
        public void CommandInput_KeyDown(object sender, KeyEventArgs e)
        {
            //If statement used to print commands entered into the console, will trime the whitespace and make it so it is not case sensitive.  
            if (e.KeyCode == Keys.Enter)
            {
                LineNumber = 0;
                String input = CommandInput.Text.Trim().ToLower(); //takes text from text box 'CommandInput' and trims the whitespace and converts it to lower case
                e.Handled = true;
                string[] splittext = input.Split(' ');

                // splits the string in input and sorts it into the indexes of the 'splittext' array
                //
                //if else statement reads the string and calls methods according to the string
                //
                if (splittext[0] == "save")
                {
                    saveImage();
                }
                //
                //draws a line from based on specified coordinates
                //
                else if (splittext[0] == "load")
                {
                    // if the lenght of the array is less than 3 it try to carry out the program
                    loadImage();
                }
                else if (splittext[0] == "compile")
                {
                    input = ProgrammingLine.Text.ToLower().Trim();
                    string[] list = input.Split('\n');
                    LineNumber = 0;
                    for (LineNumber = 0; LineNumber < list.Length; LineNumber++)
                    {
                        parse.ParseCommand(list[LineNumber]);
                    }
                }
                //
                //draws a rectangle with a predifiend measurement for the height and width
                //
                else if (splittext[0] == "exit")
                {
                    Environment.Exit(1);

                }
                //
                //drawrs a circle with a predifined radius
                //
                else
                {
                    parse.ParseCommand(input);
                }
                LineNumber = 0;
                CommandInput.Text = "";
                Refresh();
            }
        }
        private void saveImage()
        {
            ProgrammingLine.SaveFile(stream, RichTextBoxStreamType.PlainText);

            Save.CreatePrompt = true;
            Save.OverwritePrompt = true;

            Save.FileName = "newfile";

            Save.DefaultExt = "txt";
            Save.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            Save.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
            DialogResult result = Save.ShowDialog();
            Stream fileStream;

            if (result == DialogResult.OK)
            {
                fileStream = Save.OpenFile();
                stream.Position = 0;
                stream.WriteTo(fileStream);
                fileStream.Close();
            }
        }

        private void loadImage()
        {
            Load.DefaultExt = "*.txt";
            Load.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (Load.ShowDialog() == DialogResult.OK && Load.FileName.Length > 0)
            {
                ProgrammingLine.LoadFile(Load.FileName, RichTextBoxStreamType.PlainText);
            }
        }

        public void Canvass_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            graphics.DrawImageUnscaled(parse.bitmap, 0, 0);
        }

        private void ProgrammingLine_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
