﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProject
{
    class Parser
    {
        //integers for the X and Y position of the cruiser
        public int x;
        public int y;
        //integers for the width and height of the graphic image/canvas
        public int width = 529;
        public int height = 538;
        //integer to keep track of the number of liners the users has gone through
        public int LineNumber;

        public Bitmap bitmap;
        public Authenticator authenticate;
        public Color color;
        Image Finalgraphic;

        //parser method used to authenticate input and draw input onto the bitmap
        public Parser()
        {
            bitmap = new Bitmap(width, height);
            Finalgraphic = new Image(Graphics.FromImage(bitmap));
            authenticate = new Authenticator();
        }

        //Parse method to check commands and perameters have been entered correctly
        public void ParseCommand(string Line)
        {
            //counter used later for loops
            int counter;
            Line = Line.ToLower().Trim();
            //splitter used to split the command from the parameters
            string[] splitter = Line.Split(' ');
            int[] parameter;
            string command = splitter[0];
            Boolean stop = false;
            if (stop == false)
            {
                if (splitter.Length == 1 && command != "pen")
                {
                    if (command == "exit")
                    {
                        Environment.Exit(1);
                    }
                    else if (command == "fill")
                    {
                        Finalgraphic.FillShape();
                    }
                    else if (command == "clear")
                    {
                        Finalgraphic.ClearImage();
                    }
                    else if (command == "reset")
                    {
                        int xCruiser = 0;
                        int yCruiser = 0;
                        Finalgraphic.Reset(xCruiser, yCruiser);
                    }
                    else if (command == "compile")
                    {
                        LineNumber = 0;
                    }
                    else
                    {
                        MessageBox.Show("The command entered is not recongnised as a configured command!\n"
                            + "Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
                else if (splitter.Length == 2 && command != "pen")
                {
                    string[] para = splitter[1].Split(',');
                    parameter = new int[para.Length];
                    for (counter = 0; counter < para.Length; counter++)
                    {
                        try
                        {
                            parameter[counter] = int.Parse(para[counter]);
                        }
                        catch (FormatException)
                        {

                        }
                        catch (IndexOutOfRangeException)
                        {

                        }
                    }
                    if (command == "drawto")
                    {
                        if (para.Length != 2)
                        {
                            MessageBox.Show("Please enter the correct number of values.\n" + "In the format: drawto [X coordinate],[Y coordinate]",
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            CommandLine drawto = authenticate.GetCommand("drawto");
                            drawto.set(Finalgraphic, x, y, parameter[0], parameter[1]);
                            drawto.drawGraphics(Finalgraphic);
                            x = parameter[0];
                            y = parameter[1];
                        }
                    }
                    else if (command == "moveto")
                    {
                        if (para.Length != 2)
                        {
                            MessageBox.Show("Please enter the correct number of values.\n" + "In the format: moveto [X coordinate],[Y coordinate]",
                               "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            Finalgraphic.MoveTo(parameter[0], parameter[1]);
                            x = parameter[0];
                            y = parameter[1];
                        }
                    }
                    else if (command == "rectangle")
                    {
                        if (para.Length != 2)
                        {
                            MessageBox.Show("Please enter the correct number of values.\n" + "In the format: rectangle [width],[height]",
                               "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            CommandLine rectangle = authenticate.GetCommand("rectangle");
                            rectangle.set(Finalgraphic, x, y, parameter[0], parameter[1]);
                            rectangle.drawGraphics(Finalgraphic);
                        }
                    }
                    else if (command == "circle")
                    {
                        if (para.Length != 1)
                        {
                            MessageBox.Show("Please enter the correct number of values.\n" + "In the format: circle [radius]",
                               "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            CommandLine circle = authenticate.GetCommand("circle");
                            circle.set(Finalgraphic, x, y, parameter[0]);
                            circle.drawGraphics(Finalgraphic);

                        }
                    }
                    else if (command == "triangle")
                    {
                        if (para.Length != 4)
                        {
                            MessageBox.Show("Please enter the correct number of values.\n" + "In the format: triangle [value 1],[value2],[value 3],value[4]",
                               "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            CommandLine triangle = authenticate.GetCommand("triangle");
                            triangle.set(Finalgraphic, x, y, parameter[0], parameter[1], parameter[2], parameter[3]);
                            triangle.drawGraphics(Finalgraphic);
                        }
                    }
                    else
                    {
                        MessageBox.Show("The command entered is not recongnised as a configured command!\n"
                                + "Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (splitter.Length == 2 && command == "pen")
                {
                    string colorSetter = splitter[1].Trim();
                    Finalgraphic.SetColor(colorSetter);
                    this.color = Color.FromName("colorSetter");
                }
                else
                {
                    MessageBox.Show("The command entered is not recongnised as a configured command!\n"
                            + "Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            this.LineNumber++;
        }
    }
}