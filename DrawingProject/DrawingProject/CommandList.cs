﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProject
{
    interface CommandList
    {
        void drawGraphics(Image Finalgraphic);

        void set(Image newImage, params int[] line);
    }
}
