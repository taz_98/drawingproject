﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProject
{

    //Created a abstract class to prepare for intergrating individual shape classes to be parsed
    //inherting the Commanlist interface to use the draw and set
    abstract class CommandLine : CommandList
    {
        Image g;
        protected Color colour;
        protected int x, y;
        protected string[] list;

        public CommandLine()
        {
            colour = Color.Black;
            x = 0;
            y = 0;
        }

        public CommandLine(Image newImage, int[] line)
        {
            this.g = newImage;
            this.x = line[0];
            this.y = line[1];
        }

        public CommandLine(Image newImage, string[] list)
        {
            this.g = newImage;
            this.list = list;
        }

        public abstract void drawGraphics(Image Finalgraphic);

        public virtual void set(Image newImage, params int[] line)
        {
            this.x = line[0];
            this.y = line[1];
        }
    }
}
