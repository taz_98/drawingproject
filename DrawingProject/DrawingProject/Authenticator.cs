﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProject
{
    class Authenticator
    {
        public CommandLine GetCommand(string commands)
        {
            commands = commands.ToLower().Trim();

            //If the command inputted is the rectangle command then the Rectangle method is carried out
            //with the base method of the CommandLine Class
            if (commands == "rectangle")
            {
                return new Rectangle();
            }
            //If the circle command is entered then the application will follow suit but with the circle class
            else if (commands == "circle")
            {
                return new Circle();
            }
            //If the Triangle command is entered then the application will follow suit but with the Triangle class
            else if (commands == "triangle")
            {
                return new Triangle();
            }
            //If the drawto command is entered then the application will follow suit but with the drawline class
            else if (commands == "drawto")
            {
                return new DrawLine();
            }
            //In the event none of the commands match the user input a error message is display
            else
            {
                System.ArgumentException arg = new System.ArgumentException("Error!!!\n Command: "
                    + commands + " is not recognised as an command!");
                throw arg;
            }
        }
    }
}