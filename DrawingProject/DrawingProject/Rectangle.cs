﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProject
{

    //The Rectangle Class created to make the compiling of code easier when creating basic shapes
    //inherits the commandline class to use the drawgraphic method which will put the item to paper
    // also allows for the use of the set method which will set the parameters specified 
    class Rectangle : CommandLine
    {

        Image g;
        int width, height;
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }
        public override void set(Image newImage, params int[] line)
        {
            this.g = newImage;
            //this first and second integer determines the x and y position of the drawn rectangle
            base.set(newImage, line[0], line[1]);
            //this third and fourth integer determines the width of the rectangle and the length of the rectangle
            this.width = line[2];
            this.height = line[3];
        }
        public override void drawGraphics(Image Finalgraphic)
        {
            Finalgraphic.DrawRectangle(width, height);
        }
    }
}
