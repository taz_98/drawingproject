﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace DrawingProject
{
    //Constructor to set up and initialise variables to be used to draw the shapes
    public class Image
    {
        Graphics g;
        Pen myPen; // creates pen object which will be used to draw out shapes
        int Xpos, Ypos; //position of pen/cruiser
        SolidBrush fill; //creates brush used to fill graphics shapes
        bool fillbool = false; //boolean used to determine if shape is filed or drawn
        public Color color;

        public Image(Graphics g)
        {
            this.g = g; // refers to current instance of this class 
            Xpos = Ypos = 0; //Sets the pens position to 0, 0.
            fill = new SolidBrush(Color.Black); // created solid brush to fill shapes
            myPen = new Pen(Color.Black, 1); //create a pen with a black color a pizel size of 1.
            fillbool = false; //boolean set to false by default unless specified by user
        }

        public Image()
        {
            Xpos = 0;
            Ypos = 0;
            fillbool = false;
        }

        public void DrawLine(int Tox, int Toy)
        {
            g.DrawLine(myPen, Xpos, Ypos, Tox, Toy); //draws a line with the variable pen and xpos and ypos, the to x and to y sets where the position of the pen is after the line is drawn
            Xpos = Tox; //moves the xposition of the pen and keeps it there
            Ypos = Toy;//moves the yposition of the pen and keeps it there
        }
        
        public void SetColor(String color)
        {
            this.color = Color.FromName(color); //sets the color from the value of the string
            fill = new SolidBrush(Color.FromName(color)); //sets the brush to the color specified
            myPen = new Pen(Color.FromName(color)); //sets the pen to the color specified
            if ( myPen.Color.IsKnownColor == false)
            {
                MessageBox.Show("Unknown Parameter Specified", "Unknown Syntax", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            {

            }
        }

        //
        // method used to set boolean true or false based on user input
        //
        public void FillShape()
        {
            if (fillbool == true) // if the boolean has the value of 'true' it is set to false (on to off)
            {
                fillbool = false;
            }
            else if (fillbool == false) //if the boolean has the value of 'false' it is set to true (off to on)
            {
                fillbool = true;
            }
        }

        //
        //reset method used to reset the position of pen/cruiser to coordinates 0, 0
        //
        public void Reset(int resetX, int resetY)
        {
            resetX = 0;
            resetY = 0;
            Xpos = resetX;
            Ypos = resetY;
        }

        public void ClearImage()
        {
            g.Clear(Color.Transparent);
        }

        //
        //method used to set the position of pen based 
        public void MoveTo(int moveX, int moveY)
        {
            Xpos = moveX; //sets the xPos to what the user has specified
            Ypos = moveY; //sets the yPos to what the user has specified
        }

        //
        //method is used to draw line using two integers of To x and To y.
        //
       

        //
        //method is used to draw rectangle using an integer defined as size.
        //
        public void DrawRectangle(int width, int height)
        {
            if (fillbool == true) // will use brush 'fill' to fill the shape if 'fillbool' is set to true, uses the height and wdith int tp set the Xpos and Ypos.
            {
                g.FillRectangle(fill, Xpos, Ypos, width, height);
                Xpos = width;
                Ypos = height;
            }
            else // will use pen 'MyPen' to draw the outline of the shape if 'fillbool' is set to false
            {
                g.DrawRectangle(myPen, Xpos, Ypos, width, height);
                Xpos = width;
                Ypos = height;
            }
        }

        //
        //method is used to draw a circle using the interger radius and times it by 2 to work out the size of the circle.
        //
        public void DrawCircle(int radius)
        {
            if (fillbool == true) //will use brush 'fill' to fill the circle in if the value of 'fillbool' is true.
            {
                g.FillEllipse(fill, Xpos, Ypos, radius * 2, radius * 2);
            }
            else
            {
                g.DrawEllipse(myPen, Xpos, Ypos, radius * 2, radius * 2);//uses pen 'MyPen' to draw the outline of the shape if 'fillbool' is false.
            }
        }

        public void DrawTriangle(int x1, int y1, int x2, int y2) //draws 
        {
            Point[] p = new Point[3];
            p[0].X = Xpos;
            p[0].Y = Ypos;
            p[1].X = x1;
            p[1].Y = y1;
            p[2].X = x2;
            p[2].Y = y2;

            if (fillbool == true) //will fill the shape if the fillbool boolean is set to true
            {
                g.FillPolygon(fill, p);
            }
            else //will simply draw the outline of the shape if the fillbool boolean is set to false
            {
                g.DrawPolygon(myPen, p);
            }
        }
    }
}
