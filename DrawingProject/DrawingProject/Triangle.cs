﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProject
{

    //The Triangle Class created to make the compiling of code easier when creating basic shapes
    //inherits the commandline class to use the drawgraphic method which will put the item to paper
    // also allows for the use of the set method which will set the parameters specified 
    class Triangle : CommandLine
    {
        Image g;
        Point[] p = new Point[3];
        //This class is able to produce a triangle using a points array to store the integers specified
        public Triangle()
        {
            p[0].X = 100;
            p[0].Y = 100;
            p[1].X = 100;
            p[1].Y = 100;
            p[2].X = 100;
            p[2].Y = 100;
        }

        public Triangle(params int[] line)
        {
            p[0].X = line[0];
            p[0].Y = line[1];
            p[1].X = line[2];
            p[1].Y = line[3];
            p[2].X = line[4];
            p[2].Y = line[5];
        }

        public override void set(Image newImage, params int[] line)
        {
            this.g = newImage;
            base.set(newImage, line[0], line[1]);
            p[1].X = line[2];
            p[1].Y = line[3];
            p[2].X = line[4];
            p[2].Y = line[5];
        }

        public override void drawGraphics(Image Finalgraphic)
        {
            Finalgraphic.DrawTriangle(this.p[1].X, this.p[1].Y, this.p[2].X, this.p[2].Y);
        }
    }
}
