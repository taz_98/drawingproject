﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProject
{
    //The Rectangle Class created to make the compiling of code easier when creating basic shapes
    //inherits the commandline class to use the drawgraphic method which will put the item to paper
    // also allows for the use of the set method which will set the parameters specified 

    class Circle : CommandLine
    {
        int radius;

        public Circle() : base()
        {
            radius = 100;
        }

        public Circle(int x, int y, int radius)
        {
            //Only needs one variable to carry out this task.
            this.radius = radius;
        }

        public override void set(Image newImage, params int[] line)
        {
            base.set(newImage, line[0], line[0]);
            this.radius = line[2];
            //if the use specifies more than one value an error message appears.
            if (line.Length < 3 || line.Length > 3)
            {
                MessageBox.Show("Please enter the specified values format for this shape;\n" + "Circle [X coordinate], [Y coordinate] and [Radius]");
            }
        }

        public override void drawGraphics(Image newImage)
        {
            newImage.DrawCircle(radius);
        }
    }
}
